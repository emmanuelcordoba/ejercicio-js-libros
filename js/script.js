class Libro{            
    
    constructor(isbn, titulo, autor, nro_paginas){
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.nro_paginas = nro_paginas;
    }

    set setIsbn(isbn){
        this.isbn = isbn;
    }

    get getIsbn(){
        return this.isbn;
    }

    set setTitulo(titulo){
        this.titulo = titulo;
    }

    get getTitulo(){
        return this.titulo;
    }

    set setAutor(autor){
        this.autor = autor;
    }

    get getAutor(){
        return this.autor;
    }

    set setNroPaginas(nro_paginas){
        this.nro_paginas = nro_paginas;
    }

    get getNroPaginas(){
        return this.nro_paginas;
    }
}

function altaLibro() {
    let isbn = document.getElementById('isbn');
    let titulo = document.getElementById('titulo');
    let autor = document.getElementById('autor');
    let nro_paginas = document.getElementById('nro_paginas');

    //Verifico que estén todos los campos
    if(isbn.value != "" && titulo.value != "" && autor.value != "" && nro_paginas.value != ""){
        let libro = new Libro(
            isbn.value,
            titulo.value,
            autor.value,
            parseInt(nro_paginas.value)
        )
        let libros = JSON.parse(localStorage.getItem('libros'));
        
        //Controlo si no tengo aún libros almacenados
        if(!libros){
            libros = [];
        }
        libros.push(libro);
        localStorage.setItem('libros',JSON.stringify(libros));
        
        //Limpio el formulario
        isbn.value = ""
        titulo.value = ""
        autor.value = ""
        nro_paginas.value = ""
        alert('Libro guardado correctamente');
    } else {
        alert('Campos incompletos!!');
    }    
}

function mostrarLibros() {
    let libros = JSON.parse(localStorage.getItem('libros'));
    //Controlo si no tengo aún libros almacenados
    if(!libros){
        libros = [];
    }
    //Genero el contenido de la tabla
    let tabla = "";
    for (let index = 0; index < libros.length; index++) {
        let libro = libros[index];
        tabla += '<tr><td>'+libro.isbn+'</td><td>'+libro.titulo+'</td><td>'+libro.autor+'</td><td>'+libro.nro_paginas+'</td></tr>';
    }
    //Muestro el contenido de la tabla
    document.getElementById('tablaLibros').innerHTML = tabla;
}